import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Insert {

    public static void main(String[] args) {

        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection con=DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/my_db","root","root");
            String qr="insert into Emp values(124,'employee1',25000)";
            Statement stmt=con.createStatement();
            int x=stmt.executeUpdate(qr);
            if (x > 0)
                System.out.println("Successfully Inserted");
            else
                System.out.println("Insert Failed");

            con.close();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }

}
